package com.github.mchiareli.webtoken;

/**
 * WebToken operation contract
 *
 * @author maxwell chiareli
 */
public interface TokenProvider {

    /**
     * Generate a new token for the given username and permissions.
     *
     * @param username
     * @param roles
     * @return
     */
    String generate(String username, String... roles);

    /**
     * Instantiate a new user using a given token.
     *
     * @param token
     * @return
     */
    WebToken parse(String token);

}
