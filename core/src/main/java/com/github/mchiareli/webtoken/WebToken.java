package com.github.mchiareli.webtoken;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

/**
 * @author maxwell
 */
@Getter
@AllArgsConstructor
@Builder
public class WebToken {

    private final String user;

    private final Long expireAt;

    private final List<String> roles;

}
