package com.github.mchiareli.webtoken.authentication;

import com.github.mchiareli.webtoken.WebToken;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static java.util.stream.Collectors.toList;

/**
 * @author maxwell chiareli
 */
public class TokenAuthentication extends AbstractAuthenticationToken {

    private final String token;

    private final WebToken principal;

    public TokenAuthentication(String token) {
        super(null);
        this.token = token;
        this.principal = null;
        setAuthenticated(false);
    }

    public TokenAuthentication(String token, WebToken principal) {
        super(principal.getRoles().stream().map(SimpleGrantedAuthority::new).collect(toList()));
        this.token = token;
        this.principal = principal;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    public String getToken() {
        return token;
    }
}
