package com.github.mchiareli.webtoken.authentication;

import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @author maxwell chiareli
 */
public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final String header;
    private final String prefix;

    public TokenAuthenticationFilter(String defaultFilterProcessesUrl, String header, String prefix) {
        super(defaultFilterProcessesUrl);
        setAuthenticationSuccessHandler((request, response, authentication) -> {
        });
        this.header = header;
        this.prefix = prefix;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

        String token = request.getHeader(header);

        if (Objects.isNull(token)) {
            throw new InsufficientAuthenticationException("Authorization header not found");
        }

        return getAuthenticationManager().authenticate(new TokenAuthentication(token));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        // As this authentication is in HTTP header, after success we need to continue the request normally
        // and return the response as if the resource was not secured at all
        chain.doFilter(request, response);
    }

}
