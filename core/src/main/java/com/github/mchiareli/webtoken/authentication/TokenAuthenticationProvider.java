package com.github.mchiareli.webtoken.authentication;

import com.github.mchiareli.webtoken.WebToken;
import com.github.mchiareli.webtoken.TokenProvider;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * @author maxwell chiareli
 */
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private final TokenProvider tokenProvider;

    public TokenAuthenticationProvider(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String token = ((TokenAuthentication) authentication).getToken();
        WebToken webToken = tokenProvider.parse(token);

        return new TokenAuthentication(token, webToken);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(TokenAuthentication.class);
    }

}
