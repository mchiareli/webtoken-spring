package com.github.mchiareli.webtoken.jwt;

import com.github.mchiareli.webtoken.WebToken;
import com.github.mchiareli.webtoken.TokenProvider;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static io.jsonwebtoken.Jwts.builder;
import static io.jsonwebtoken.Jwts.claims;
import static java.util.Arrays.stream;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

/**
 * JSON Web WebToken implementation for {@link TokenProvider}.
 *
 * @author maxwell chiareli
 */
public class JWTProvider implements TokenProvider {

    private static final Logger log = LoggerFactory.getLogger(JWTProvider.class);

    private final SecretKey secret;
    private final SignatureAlgorithm algorithm;

    public JWTProvider(SignatureAlgorithm algorithm, SecretKey secret) {
        this.secret = secret;
        this.algorithm = algorithm;
    }

    @Override
    public String generate(String username, String... roles) {

        Claims claims = claims().setSubject(username);

        if (nonNull(roles) && roles.length > 0) {
            claims.put("roles", String.join(",", roles));
        }

        String token = builder()
                .setClaims(claims)
                .signWith(algorithm, secret)
                .compact();

        log.info("new token was generated: {}", token);

        return token;
    }

    @Override
    public WebToken parse(String token) {

        Claims body = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();

        List<String> roles = Optional.ofNullable((String) body.get("roles"))
                .map(r -> Arrays.stream(r.split(",")))
                .orElse(Stream.empty())
                .collect(toList());

        return WebToken.builder()
                .user(body.getSubject())
                .roles(roles)
                .expireAt(Optional.ofNullable(body.getExpiration()).map(Date::getTime).orElse(null))
                .build();
    }

}
