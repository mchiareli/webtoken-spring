package com.github.mchiareli.webtoken.jwt;

import com.github.mchiareli.webtoken.WebToken;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 * @author maxwell
 */
@RunWith(MockitoJUnitRunner.class)
public class JWTProviderTest {

    private String username;
    private JWTProvider jwtProvider;

    @Before
    public void setup() {
        username = "joe.doe";
        jwtProvider = new JWTProvider(SignatureAlgorithm.HS512, MacProvider.generateKey());
    }

    @Test
    public void itShouldGenerateValidTokenForEmptyRole() {
        String token = jwtProvider.generate(username);
        assertNotNull(token);
        WebToken webWebToken = jwtProvider.parse(token);
        assertNotNull(webWebToken);
        assertEquals(username, webWebToken.getUser());
        Collection<String> roles = webWebToken.getRoles();
        assertNotNull(roles);
        assertEquals(0, roles.size());
    }

    @Test
    public void itShouldGenerateValidTokenForSingleRole() {
        String token = jwtProvider.generate(username, "ADMIN");
        assertNotNull(token);
        WebToken webWebToken = jwtProvider.parse(token);
        assertNotNull(webWebToken);
        assertEquals(username, webWebToken.getUser());
        Collection<String> roles = webWebToken.getRoles();
        assertNotNull(roles);
        assertEquals(1, roles.size());
        assertTrue(roles.contains("ADMIN"));
    }

    @Test
    public void itShouldGenerateValidTokenForMultipleRole() {
        String token = jwtProvider.generate(username, "ADMIN", "SUPER");
        assertNotNull(token);
        WebToken webWebToken = jwtProvider.parse(token);
        assertNotNull(webWebToken);
        assertEquals(username, webWebToken.getUser());
        Collection<String> roles = webWebToken.getRoles();
        assertNotNull(roles);
        assertEquals(2, roles.size());
        assertTrue(roles.contains("ADMIN"));
        assertTrue(roles.contains("SUPER"));
    }

}