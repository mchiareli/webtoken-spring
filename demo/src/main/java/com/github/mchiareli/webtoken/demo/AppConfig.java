package com.github.mchiareli.webtoken.demo;

import com.github.mchiareli.webtoken.TokenProvider;
import com.github.mchiareli.webtoken.authentication.TokenAuthenticationFilter;
import com.github.mchiareli.webtoken.authentication.TokenAuthenticationProvider;
import com.github.mchiareli.webtoken.jwt.JWTProvider;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

/**
 * @author maxwell
 */
@Configuration
public class AppConfig {

    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class Security extends WebSecurityConfigurerAdapter {

        @Bean
        public TokenAuthenticationFilter authenticationTokenFilterBean() throws Exception {
            TokenAuthenticationFilter authenticationTokenFilter = new TokenAuthenticationFilter("/api/**", "Authorization", "Bearer ");
            authenticationTokenFilter.setAuthenticationManager(authenticationManagerBean());
            return authenticationTokenFilter;
        }

        @Bean
        public AuthenticationProvider authenticationProvider() {
            return new TokenAuthenticationProvider(tokenProvider());
        }

        @Bean
        public TokenProvider tokenProvider() {
            return new JWTProvider(SignatureAlgorithm.HS512, MacProvider.generateKey());
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(authenticationProvider());
        }

        @Override
        protected void configure(HttpSecurity httpSecurity) throws Exception {

            httpSecurity
                    .csrf().disable()
                    .sessionManagement().sessionCreationPolicy(STATELESS)
                    .and()
                    .authorizeRequests()
                    .antMatchers("/api/user/**").hasRole("USER")
                    .antMatchers("/api/super/**").hasRole("SUPER")
                    .antMatchers("/api/**").authenticated()
                    .and()
                    .addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class)
                    .headers().cacheControl();
        }

    }

}
