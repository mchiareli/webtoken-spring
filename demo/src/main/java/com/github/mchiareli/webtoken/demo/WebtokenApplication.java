package com.github.mchiareli.webtoken.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author maxwell
 */
@SpringBootApplication
public class WebtokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebtokenApplication.class, args);
    }

}
