package com.github.mchiareli.webtoken.demo.resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author maxwell chiareli
 */
@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class PingResource {

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String ping() {
        return "pong";
    }

    @RequestMapping(value = "/user/ping", method = RequestMethod.GET)
    public String userPing() {
        return "pong";
    }

    @RequestMapping(value = "/super/ping", method = RequestMethod.GET)
    public String superPing() {
        return "pong";
    }

}
