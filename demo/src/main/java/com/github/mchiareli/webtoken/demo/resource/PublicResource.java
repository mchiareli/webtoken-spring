package com.github.mchiareli.webtoken.demo.resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author maxwell chiareli
 */
@RestController
@RequestMapping(value = "/public", produces = MediaType.APPLICATION_JSON_VALUE)
public class PublicResource {

    @RequestMapping("/about")
    public String about() {
        return "about";
    }

}
