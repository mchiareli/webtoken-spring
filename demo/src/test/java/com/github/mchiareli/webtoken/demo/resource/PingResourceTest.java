package com.github.mchiareli.webtoken.demo.resource;

import com.github.mchiareli.webtoken.TokenProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author maxwell chiareli
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PingResourceTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    TokenProvider tokenProvider;

    @Test
    public void shouldAllowAccessForLogged() {
        HttpHeaders httpHeaders = testRestTemplate.headForHeaders("/api/ping");
        String token = tokenProvider.generate("joe.doe");
        httpHeaders.add("Authorization", token);
        ResponseEntity<String> responseEntity = testRestTemplate.exchange("/api/ping", HttpMethod.GET, new HttpEntity<Object>(httpHeaders), String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void shouldAllowAccessForLoggedUser() {
        HttpHeaders httpHeaders = testRestTemplate.headForHeaders("/api/user/ping");
        String token = tokenProvider.generate("joe.doe", "ROLE_USER");
        httpHeaders.add("Authorization", token);
        ResponseEntity<String> responseEntity = testRestTemplate.exchange("/api/user/ping", HttpMethod.GET, new HttpEntity<Object>(httpHeaders), String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void shouldAllowAccessForLoggedSuperUser() {
        HttpHeaders httpHeaders = testRestTemplate.headForHeaders("/api/super/ping");
        String token = tokenProvider.generate("joe.doe", "ROLE_SUPER");
        httpHeaders.add("Authorization", token);
        ResponseEntity<String> responseEntity = testRestTemplate.exchange("/api/super/ping", HttpMethod.GET, new HttpEntity<Object>(httpHeaders), String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void shouldDenyAccessForProtectedResource() {
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("/api/ping", String.class);
        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
    }

    @Test
    public void shouldDenyAccessForAccessNotGranted() {
        HttpHeaders httpHeaders = testRestTemplate.headForHeaders("/api/super/ping");
        String token = tokenProvider.generate("joe.doe", "ROLE_USER");
        httpHeaders.add("Authorization", token);
        ResponseEntity<String> responseEntity = testRestTemplate.exchange("/api/super/ping", HttpMethod.GET, new HttpEntity<Object>(httpHeaders), String.class);
        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    }

}