package com.github.mchiareli.webtoken.demo.resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * @author maxwell chiareli
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PublicResourceTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldAccessPublicResource() {
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("/public/about", String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

}